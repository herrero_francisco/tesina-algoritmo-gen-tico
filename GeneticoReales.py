import sys
sys.path.append(sys.path[0]+'\\Reales')
import R_Metodos_Utiles
import R_Operadores
import R_Funciones
import R_Data_Functions
import abstractClass
import random
import json
import main
from time import time

"""
Clase de Resultados Obtenidos
"""
class ResultadosProblemaOptimizacionFunciones():
    def __init__(self):
        abstractClass.Resultados.__init__(self)

"""
Clase Encoder de Resultados Obtenidos
"""
class ResultadosProblemaOptimizacionFuncionesEncoder(json.JSONEncoder):
    def default(self, obj):
        return obj.__dict__

"""
Clase Concreta para el Problema de Optimización de Funciones
"""
class ProblemaOptimizacionFunciones:
    def __init__(self, tipo_funcion, tam_dimension, limite_inferior_dominio, limite_superior_dominio):
        self.tam_dimension= tam_dimension
        self.tipo_funcion= tipo_funcion
        self.limite_inferior_dominio= limite_inferior_dominio
        self.limite_superior_dominio= limite_superior_dominio

    def estado_aleatorio(self):
        """
        Genera un individuo que contiene tantos elementos como sea el tamaño de la dimensión
        (genera un vector aleatorio en R^n)
        """
        lista=list()
        for i in range(self.tam_dimension):
            lista.append(random.uniform(self.limite_inferior_dominio, self.limite_superior_dominio))
        return tuple(lista)

    def evaluar_en_funcion(self, elemento):
        """
        Función que se encarga de evaluar el fitness del elemento, en base a la función que se ha solicitado
        ----------------
        @param elemento: individuo a ser evaluado.
        ----------------
        @return fitness del elemento
        """
        fitness= R_Funciones.functions(self.tipo_funcion, self.tam_dimension, elemento)
        return fitness

"""
Clase Concreta para el Algoritmo Genético Real
"""
class GeneticoReales():
    def __init__(self, problema, tam_poblacion, nro_generaciones, nro_padres, op_seleccion, op_cruce, prob_cruce, op_mutacion, prob_mutacion, op_reemplazo, op_seleccion_reemplazo, elitismo, lambda_cantidad_hijos, valor_alfa):
        """
        @param problema: Objeto problema a ser resuelto por el algoritmo genético
        @param nro_generaciones: Número de generaciones a ser corridas en la ejecución del GA
        @param nro_padres: Cantidad de padres a ser elegidos para realizar el cruzamiento
        @param op_seleccion: Operador de Selección a ser utilizado
        @param op_cruce: Operador de Cruce a ser utilizado
        @param prob_cruce: Probabilidad de que ocurra el cruzamiento [0.45;0.95] --> Rango más usado
        @param op_mutacion: Operador de Mutación a ser utilizado
        @param prob_mutacion: Probabilidad de que ocurra la mutación del individuo [0.001;0.01] --> Rango más usado
        @param op_reemplazo: Operador de Reemplazo a ser utilizado
        @param lambda_cantidad_hijos= cantidad de hijos a ser generados en cada generación
        @param valor_alfa: utilizado para realizar el cruzamiento intermedio
        """
        self.cantidad_evaluaciones_funcion_fitness = 0
        self.problema= problema
        self.nro_padres= nro_padres
        self.op_seleccion= op_seleccion
        self.op_cruce= op_cruce
        self.prob_cruce= prob_cruce
        self.op_mutacion= op_mutacion
        self.prob_mutacion= prob_mutacion
        self.op_reemplazo= op_reemplazo
        self.op_seleccion_reemplazo= op_seleccion_reemplazo
        self.lambda_cantidad_hijos=lambda_cantidad_hijos
        self.valor_alfa=valor_alfa
        self.inicializa_poblacion(tam_poblacion, elitismo)

    def inicializa_poblacion(self, tam_poblacion, elitismo):
        """
        Inicializa la población para el algoritmo genético
        ---------------------
        @param tam_poblacion: tamaño de la población generada
        @param elitismo: indica si se va a hacer uso de elitismo para el reemplazo poblacional
        ---------------------
        @return: None

        NOTA: Internamente guarda self.tam_poblacion y self.poblacion
        """
        self.tam_poblacion=tam_poblacion
        individuos=[self.estado_a_cadena(self.problema.estado_aleatorio())
                    for _ in range(tam_poblacion)]
        self.poblacion=[(self.adaptacion(individuo), individuo)
                        for individuo in individuos]

        if(elitismo):
            self.posicion_mejor_individuo= self.poblacion.index(min(self.poblacion))
        else:
            self.posicion_mejor_individuo= -1

    @staticmethod
    def estado_a_cadena(estado):
        """
        Convierte un estado a una cadena de cromosomas
        --------------
        @param estado: una tupla con un estado
        --------------
        @return: una lista con una cadena de caracteres

        NOTA: Por defecto, convierte el estado en una lista
        """
        return list(estado)

    @staticmethod
    def cadena_a_estado(cadena):
        """
        Convierte una cadena de cromosomas a un estado
        --------------
        @Param cadena: una lista de cromosomas o valores
        --------------
        @Return: una tupla con un estado válido
        
        NOTA: Por defecto convierte la lista a tupla
        
        """
        return tuple(cadena)

    def adaptacion(self,individuo):
        """
        Calcula la adaptación (fitness) de un individuo al medio, mientras más adaptado
        mejor, mayor costo, menor adaptación.
        --------------------
        @Param individuo: lista de cromosomas a ser evaluados
        --------------------
        @Return: fitness
        """
        self.cantidad_evaluaciones_funcion_fitness+=1
        #Preguntar si está bien, o hay que cambiarlo#
        fitness=self.problema.evaluar_en_funcion(individuo)
        return fitness
    
    def busqueda(self, nro_generaciones):
        """
        Algoritmo genético general
        Este método es el que se encarga de realizar el ciclo de vida del
        algoritmo genético en si. (Se le puede cambiar el nombre...)
        ------------------------
        @param nro_generaciones: Número de generaciones a simular
        ------------------------
        @return: Un estado del problema
        """
        resultados= ResultadosProblemaOptimizacionFunciones()
        flag=1
        self.solucion_encontrada=""
        tiempo_inicial=time()
        tiempo_mejor_solucion=time()
        resultados.mejor_solucion_inicial= round((min(self.poblacion)[0]),3)
        mejor_solucion= (min(self.poblacion)[0])
        generacion_mejor_solucion=0

        resultados.peor_solucion_inicial= round((max(self.poblacion)[0]),3)
        peor_solucion=max(self.poblacion)[0]
        #Cálculo del Promedio
        suma= 0.0
        for ind in self.poblacion:
            suma=suma+ind[0]
        avg=suma/self.tam_poblacion
        resultados.promedio_solucion_inicial= round(avg,3)
        #Corrida de las generaciones
        for i in range(nro_generaciones):
            if(mejor_solucion>(min(self.poblacion))[0]):
                mejor_solucion=(min(self.poblacion))[0]
                tiempo_mejor_solucion=time()
                generacion_mejor_solucion=i
            #Armado de las parejas de padres
            indices_parejas= self.seleccion()
            #Cruza de los padres y obtención de los hijos
            hijos= self.cruza(indices_parejas)
            #Mutación de los hijos obtenidos
            self.mutacion(hijos)
            #Reemplazo poblacional con los hijos
            self.reemplazo(hijos)
        #Finaliza for
        tiempo_final=time()
        tiempo_total=tiempo_final-tiempo_inicial
        resultados.tiempo_total_ejecucion= round(tiempo_total,3)
        resultados.mejor_solucion=round(mejor_solucion,3)
        resultados.generacion_mejor_solucion=generacion_mejor_solucion
        resultados.tiempo_mejor_solucion= round((tiempo_mejor_solucion-tiempo_inicial),3)
        resultados.cantidad_evaluaciones_funcion_fitness=self.cantidad_evaluaciones_funcion_fitness
        resultados.total_generaciones=nro_generaciones
        resultados.mejor_solucion_final=round((min(self.poblacion)[0]),3)
        resultados.peor_solucion_final=round((max(self.poblacion)[0]),3)
        suma=0.0
        for ind in self.poblacion:
            suma=suma+ind[0]
        avg=suma/self.tam_poblacion
        resultados.promedio_solucion_final= round(avg,3)
        distancia=R_Metodos_Utiles.distancia_Relativa_Optimo(mejor_solucion, R_Data_Functions.f_bias[self.problema.tipo_funcion-1])
        error=R_Metodos_Utiles.error_Respecto_Optimo(distancia)
        resultados.diferencia_relativa_optimo=distancia
        resultados.error_relativo=error
        resultados.optimo=R_Data_Functions.f_bias[self.problema.tipo_funcion-1]
        return (resultados.__dict__)

    def seleccion(self):
        """
        Método que se encarga de llamar al Operador indicado en la opción
        que se ha cargado previamente
        ----------------
        @Param self: referencia al objeto
        ----------------
        @Return padres: lista de indices de los individuos de la población
        que fueron seleccionados como padres
        """
        padres=[]
        #Suma de Aptitudes de la población actual
        suma_aptitudes=1.0*sum(pop[0] for pop in self.poblacion)
        #Fitness relativo de los individuos en base a la suma de aptitudes anterior
        fitness_relativo=[(abs(pop[0]/suma_aptitudes)) for pop in self.poblacion]

        #Si se quiere Usar Ruleta para la selección de Padres
        if(self.op_seleccion==0):
            padres=[R_Operadores.seleccion_Ruleta(self.poblacion, fitness_relativo)
                for _ in range(self.nro_padres)]
            self.indice_padres= padres
        #print(padres)

        #Si se quiere usar SUS para la selección de Padres
        if(self.op_seleccion==1):
            padres= R_Operadores.seleccion_SUS(self.poblacion, self.nro_padres, fitness_relativo)
            self.indice_padres= padres
        #print(padres)

        #Si se quiere usar Torneo Binario para la selección de Padres
        if(self.op_seleccion==2):
            padres=[R_Operadores.seleccion_Torneo_Binario(self.poblacion, self.tam_poblacion)
                for _ in range(self.nro_padres)]
            self.indice_padres= padres

        return R_Metodos_Utiles.armar_parejas(padres)

    def cruza(self, padres):
        """
        Método que se encarga de realizar el Cruzamiento, teniendo en cuenta
        el tipo de cruce que es utilizado, y la probabilidad de que ocurra el
        cruzamiento entre padres.

        ----------
        @Param padres: array que contiene la posición en la población de los padres
        a ser cruzados

        @Return offsprings: array de hijos generados (ver si se le asigna fitness cero, en caso de éxito)
        """
        offsprings=[]

        if self.op_cruce==0:
            #Se utiliza recombinación discreta
            for (i,j) in padres:
                #Recorro las parejas
                aleatorio=random.uniform(0,1)
                if(aleatorio<self.prob_cruce):
                    offsprings.append(R_Operadores.recombinacion_Discreta(self.poblacion[i][1], self.poblacion[j][1], self.problema.tam_dimension))
                else:
                    aleatorio=random.uniform(0,1)
                    if(aleatorio<0.5):
                        offsprings.append(self.poblacion[i][1])
                    else:
                        offsprings.append(self.poblacion[j][1])
                
                if(len(offsprings)==self.lambda_cantidad_hijos):
                    break
                elif(len(offsprings)>self.lambda_cantidad_hijos):
                    offsprings.pop()
                    break

        if self.op_cruce==1:
            #Se utiliza recombinación intermedia
            for (i,j) in padres:
                aleatorio=random.uniform(0,1)
                if(aleatorio<self.prob_cruce):
                    #offsprings.append(R_Operadores.recombinacion_Intermedia(self.poblacion[p1][1], self.poblacion[p2][1], self.problema.tam_dimension, 0.35))
                    lista=R_Operadores.recombinacion_Intermedia(self.poblacion[i][1], self.poblacion[j][1], self.problema.tam_dimension, self.valor_alfa)
                    offsprings.append(lista[0])
                    offsprings.append(lista[1])
                else:
                    offsprings.append(self.poblacion[i][1])
                    offsprings.append(self.poblacion[j][1])
                
                if(len(offsprings)==self.lambda_cantidad_hijos):
                    break
                elif(len(offsprings)>self.lambda_cantidad_hijos):
                    offsprings.pop()
                    break
                
        if self.op_cruce==2:
            #Se utiliza Recombinación Lineal
            for(i,j) in padres:
                aleatorio=random.uniform(0,1)
                if(aleatorio<self.prob_cruce):
                    offsprings.append(R_Operadores.recombinacion_Lineal(self.poblacion[i][1], self.poblacion[j][1]))
                else:
                    aleatorio=random.uniform(0,1)
                    if(aleatorio<0.5):
                        offsprings.append(self.poblacion[i][1])
                    else:
                        offsprings.append(self.poblacion[j][1])
                
                if(len(offsprings)==self.lambda_cantidad_hijos):
                    break
                elif(len(offsprings)>self.lambda_cantidad_hijos):
                    offsprings.pop()
                    break

        if self.op_cruce==3:
            for(i,j) in padres:
                aleatorio=random.uniform(0,1)
                if(aleatorio<self.prob_cruce):
                    offsprings.append(R_Operadores.recombinacion_Aritmetica(self.poblacion[i][1], self. poblacion[j][1]))
                else:
                    aleatorio=random.uniform(0,1)
                    if(aleatorio<0.5):
                        offsprings.append(self.poblacion[i][1])
                    else:
                        offsprings.append(self.poblacion[j][1])
                    
                if(len(offsprings)==self.lambda_cantidad_hijos):
                    break
                elif(len(offsprings)>self.lambda_cantidad_hijos):
                    offsprings.pop()
                    break
        
        return offsprings

    def mutacion(self, hijos):
        """
        Método que se encarga de realizar la Mutación, teniendo en cuenta
        el tipo de mutación que es utilizado, y la probabilidad de que ocurra la
        mutación de los hijos que se obtuvieron.

        --------------
        @Param hijos: array que contiene los hijos obtenidos del cruzamiento
        entre los padres seleccionados
        --------------
        @Return: array de hijos mutados
        """
        if self.op_mutacion==0:
            for i in range(len(hijos)):
                aleatorio=random.uniform(0,1)
                if(aleatorio<self.prob_mutacion):
                    R_Operadores.mutacion_Aleatoria_Uniforme(self, hijos[i])

        if self.op_mutacion==1:
            for i in range(len(hijos)):
                aleatorio=random.uniform(0,1)
                if(aleatorio<self.prob_mutacion):
                    R_Operadores.mutacion_Gaussiana(self, hijos[i])

    def reemplazo(self, hijos):
        if self.op_reemplazo==0:
            R_Operadores.reemplazo_mu_mas_lambda(self, hijos)

        if self.op_reemplazo==1:
            R_Operadores.reemplazo_mu_lambda(self, hijos)

        if self.op_reemplazo==2:
            R_Operadores.reemplazo_Generacional(self,hijos)
        