import math
import R_Data_Functions

"""
Definición de parámetros
@param pi
@param e
"""
#pi=math.pi
pi=math.acos(-1.0)
#e=math.e
e=math.exp(1.0)

def functions(function_number, dimention, element):
    """
    Función que contiene las funciones implementadas y a ser optimizadas
    Se encarga de llamar al método correspondiente

    @param function_number: entero que indica la función a ser utilizada.
    Los valores permitidos para este parámetro son:
        1--> Función Shifted Sphere
        2--> Función Schwefel
        3--> Función Shifted Rosenbrock
        4--> Función Shifted Rastrigin
        5--> Función Shifted Griewank
        6--> Función Shifted Ackley
    @param dimention: dimensión en que se encuentra el elemento
    @param elemento: individuo a ser evaluado

    @return: evaluación del elemento
    """
    eval=0.0
    if(dimention>1000):
        dimention=1000

    if(function_number==1):
        eval=shifted_Sphere(dimention, element)
    if(function_number==2):
        eval= schwefel_Problem(dimention, element)
    if(function_number==3):
        eval=shifted_Rosenbrock(dimention, element)
    if(function_number==4):
        eval=shifted_Rastrigin(dimention, element)
    if(function_number==5):
        eval=shifted_Griewank(dimention, element)
    if(function_number==6):
        eval=shifted_Ackley(dimention, element)
    if(function_number<1 or function_number >6):
        raise ValueError("Esto NO debe pasar!")

    return eval

def shifted_Sphere(dim, elem):
    z=0.0
    f=0.0
    for i in range(dim):
        z= elem[i]-R_Data_Functions.sphere[i]
        f=f+(z*z)

    return (f+R_Data_Functions.f_bias[0])

def schwefel_Problem(dim, elem):
    z=0.0
    f=abss(elem[0]-R_Data_Functions.schwefel[0])
    for i in range(1,dim):
        z=elem[i]-R_Data_Functions.schwefel[i]
        f=max(f,abss(z))

    return (f+R_Data_Functions.f_bias[1])

def shifted_Rosenbrock(dim, elem):
    z=[]
    f=0.0
    for i in range(dim):
        res= elem[i]-R_Data_Functions.rosenbrock[i]+1
        z.append(res)

    for i in range(dim-1):
        """tmp= math.pow(z[i],2)-z[i+1]
        tmp2= math.pow(tmp,2)
        tmp3= math.pow((z[i]-1),2)
        f=f+100*tmp2+tmp3"""
        f=f+100*(math.pow((math.pow(z[i],2)-z[i+1]),2))+math.pow((z[i]-1),2)
    
    return (f+R_Data_Functions.f_bias[2])

def shifted_Rastrigin(dim, elem):
    z=0.0
    f=0.0
    for i in range(dim):
        z=elem[i]-R_Data_Functions.rastrigin[i]
        f=f+(math.pow(z,2)-10*math.cos(2 * pi * z)+10)

    return (f+R_Data_Functions.f_bias[3])

def shifted_Griewank(dim, elem):
    z=0.0
    f1=0.0
    f2=1.0
    for i in range(dim):
        z=elem[i]-R_Data_Functions.griewank[i]
        f1=f1+(math.pow(z,2)/4000)
        f2=f2*(math.cos(z/math.sqrt(i+1)))

    return (f1 - f2 + 1 + R_Data_Functions.f_bias[4])

def shifted_Ackley(dim, elem):
    z=0.0
    sum1=0.0
    sum2=0.0
    f=0.0
    for i in range(dim):
        z= elem[i] - R_Data_Functions.ackley[i]
        sum1= sum1 + math.pow(z,2)
        sum2= sum2 + math.cos(2 * pi * z)

    f= -20 * math.exp(-0.2 * math.sqrt(sum1/dim)) - math.exp(sum2/dim) + 20 + e + R_Data_Functions.f_bias[5]

    return (f)

def abss(value):
    if(value<0):
        return -value
    else:
        return value