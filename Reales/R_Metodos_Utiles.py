import random

"""
Método que genera los pares de indices de padres
"""
def armar_parejas(padres):
    """
    Dado un array de posiciones de padres, arma tuplas que representan
    las parejas de padres con las que luego se va a hacer el cruce
    ------------------
    @Param padres: vector que contiente las posiciones de los padres
    en la población
    ------------------
    @Return lista: lista de tuplas donde cada tupla es una pareja de padres
    ------------------
    EJEMPLO:
        padres = [1,4,7,5,0,8]
        lista = [(1,4),(7,5),(0,8)]
    """
    lista=[]
    j=1
    i=0
    while(j < len (padres)):
        tupla = (padres[i], padres[j])
        lista.append(tupla)
        i+=2
        j+=2
    return lista

"""
Método para obtener la distancia relativa al óptimo respecto el obtenido
"""
def distancia_Relativa_Optimo(optimo_encontrado, optimo_global):
    """
    Se calcula la distancia relativa en base al óptimo obtenido
    respecto al óptimo global. El cálculo es:
    |óptimo-encontrado|/óptimo
    """
    distancia= 0.0
    numerador=abs(optimo_global - optimo_encontrado)
    distancia= numerador / abs(optimo_global)
    return (round(distancia,3))

"""
Método para obtener el porcentaje de error respecto al óptimo
"""
def error_Respecto_Optimo(distancia):
    """
    Se calcula el porcentaje de error del óptimo encontrado
    respecto al óptimo global. El cálculo es:
    distancia relativ * 100
    """
    error= distancia*100
    return (round(error,3))
