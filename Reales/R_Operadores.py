import random
import math

"""
OPERADORES DE SELECCIÓN DE PADRES
"""
def seleccion_Ruleta(poblacion, fitness_relativo):
    """
    seleccion_Ruleta(poblacion)

    De toda la población, selecciona un individuo aleatorio
    elegido aleatoriamente de una ruleta donde está 
    distribuida proporcionalmente las aptitudes de los 
    individuos.

    Parameters
    ----------
    poblacion : Array que representa la
    poblacion del objeto GeneticoReal

    Returns
    -------
    posición donde se encuentra el individuo elegido del array
    población.

    """
    pos=random.uniform(0,1)
    acumulado=0
    #suma_aptitudes= 1.0 * sum([ind[0] for ind in poblacion])
    for i in range(len(poblacion)):
        acumulado+= fitness_relativo[i]
        if(pos <= acumulado):
            return i
    raise ValueError("Esto NO tiene que pasar!")

def seleccion_SUS(poblacion, nro_padres, fitness_relativo):
    """
    seleccion_SUS(poblacion)

    De toda la población,...

    Parameters
    ----------
    poblacion: Array que representa la población del objeto GeneticoReal
    nro_padres: cantidad de padres a ser seleccionados de la población

    Returns
    array de Padres seleccionados
    -------
    """
    #Creación de la rueda
    rueda= makeWheel(poblacion, fitness_relativo)
    stepSize = 1.0/nro_padres
    padres=[]
    aleatorio=random.uniform(0,1)
    padres.append(binSearch(rueda, aleatorio))
    while len(padres) < nro_padres:
        aleatorio+=stepSize
        if aleatorio>1:
            aleatorio%=1
        padres.append(binSearch(rueda, aleatorio))
    return padres

def seleccion_Torneo_Binario(poblacion, longitud_Poblacion):
    """
    seleccion_Torneo_Binario(self)

    De toda la población, selecciona 2 individuos aleatorios
    y se queda con el que tiene mejor fitness de ellos.

    Parameters
    ----------
    self : objeto GeneticoReal

    Returns
    -------
    posición donde se encuentra el individuo elegido del array
    población.
    
    """
    aleatorio1= random.randint(0,longitud_Poblacion-1)
    aleatorio2= random.randint(0,longitud_Poblacion-1)

    return(aleatorio1
           if poblacion[aleatorio1][0] > poblacion[aleatorio2][0]
           else aleatorio2)

"""
OPERADORES DE CRUCE DE PADRES
"""
def recombinacion_Discreta(padre1, padre2, tam_dimension):
    """
    El cruce de padres se hace de la siguiente forma:
    Dados dos padres se eligen los cromosomas de uno o de otro, de acuerdo
    a un aleatorio entre 0 y 1, donde:
    si aleatorio <= 0.5, entonces se elige el cromosoma del padre 1
    caso contrario, se elige el cromosoma del padre 2

    Parameters
    ----------
    padre1: individuo que actúa de padre
    padre2: ídem padre1

    Returns
    -------
    Individuo Hijo/Offspring, resultante del cruzamiento
    """
    #Mejorar la parte del for (agregar parámetro extra, para más generalización)
    offspring=[]
    for i in range(tam_dimension):
        aleatorio= random.uniform(0,1)
        if(aleatorio<=0.5):
            offspring.append(padre1[i])
        else:
            offspring.append(padre2[i])

    return offspring

def recombinacion_Intermedia(padre1, padre2, tam_dimension, alfa):
    """
    Dado un determinado valor alfa, offspring es generando de la siguiente forma:
    suma del gen del padre1 * alfa, más el gen del padre2 * (1-alfa)
    Este crossover es una generalización del crossover aritmético, donde
    alfa = 0.5

    Parameters
    ----------
    padre1: individuo que actúa de padre
    padre2: ídem padre1

    Returns
    -------
    Individuo Hijo/Offspring, resultante del cruzamiento
    """
    offsprings=[]
    offspring1=[]
    offspring2=[]
    for i in range(tam_dimension):
        gen1= (alfa * padre1[i]) + ((1 - alfa) * padre2[i])
        offspring1.append(gen1)
        gen2= ((1-alfa)*padre1[i])+(alfa*padre2[i])
        offspring2.append(gen2)
    
    offsprings.append(offspring1)
    offsprings.append(offspring2)

    return offsprings

def recombinacion_Lineal(padre1, padre2):
    """
    Se calcula cada gen del hijo como la raíz cuadrada de la multiplicación de los respectivos genes de sus padres.

    Parameters
    ----------
    padre1: individuo que actúa de padre
    padre2: ídem padre1

    Returns
    -------
    Individuo Hijo/Offspring, resultante del cruzamiento
    """
    offspring=[]
    for i in range(len(padre1)):
        gen= math.sqrt(abs(padre1[i] * padre2[i]))
        offspring.append(gen)

    return offspring

def recombinacion_Lineal_Intermedia(padre1, padre2):
    raise NotImplementedError("Falta Desarrollar")

def recombinacion_Aritmetica(padre1, padre2):
    """
    alfa = 0.5, por lo que el offspring es generado de la siguiente forma:
    (gen padre1 + gen padre2) * 0.5 o
    (gen padre1 + gen padre2) / 2

    Parameters
    ----------
    padre1: individuo que actúa de padre
    padre2: ídem padre1

    Returns
    -------
    Individuo Hijo/Offspring, resultante del cruzamiento
    """
    offspring=[]
    for i in range(len(padre1)):
        alelo=(padre1[i] + padre2[i]) / 2
        offspring.append(alelo)

    return offspring

"""
OPERADORES DE MUTACIÓN DE INDIVIDUO
"""
def mutacion_Aleatoria_Uniforme(self, individuo):
    """
    Dado un individuo y los límites superior e inferior
    del dominio, se cambia el valor del gen por un valor
    aleatorio dentro del dominio del problema. Cada gen tiene
    1/len(individuo) de ser elegido
    """
    min= self.problema.limite_inferior_dominio
    max= self.problema.limite_superior_dominio
    p= 1 / len(individuo)
    for i in range(len(individuo)):
        aleatorio= random.uniform(0,1)
        if(aleatorio <= p):
            individuo[i]= random.uniform(min, max)

    return individuo

def mutacion_Gaussiana(self, individuo):
    """
    Perturbación de los valores del individuo
    mediante una distribución Gaussiana
    -----------------
    @Param individuo: el individuo que va a ser mutado
    -----------------
    @Return: el individuo que fue mutado
    -----------------
    NOTA: sigma puede tomar tres valores: 0.005, 0.02 o 0.1 (Essentials Of Maetaheuristics, página 27,
    Algoritmo 11 'Gaussian Convolution')
    """
    min=self.problema.limite_inferior_dominio
    max=self.problema.limite_superior_dominio
    p=1/(len(individuo))
    n=0.0
    for i in range(len(individuo)):
        aleatorio=random.uniform(0,1)
        if(p>=aleatorio):
            while True:
                n=random.gauss(0, 0.1)
                if(min<=individuo[i]+n and individuo[i]+n<=max):
                    break
                
            individuo[i]=individuo[i]+n

    return individuo
    
"""
OPERADORES DE REEMPLAZO POBLACIONAL
"""
def reemplazo_Generacional(self, individuos):
    """
    Consiste en reemplazar completamente la población de padres,
    por la población de hijos obtenidos. En caso de que los hijos obtenidos sean menor
    que la cantidad total de la población, lo que debe hacerse es copiar tantas veces los
    hijos como sea necesario para completar el tamaño de población, por lo que habrá
    individuos repetidos
    --------------------
    @Param self: referencia al objeto GA Real
    @Param individuos: array que contiene a los hijos obtenidos del cruzamiento entre padres
    --------------------
    @Return none
    """
    individuos=[(self.adaptacion(individuo), individuo)
                for individuo in individuos]
    tam_aux_Pop= self.tam_poblacion
    i=0
    j=0
    #Se verifica si se hace uso de Elitismo
    if(self.posicion_mejor_individuo!=-1):
        mejor_individuo= self.poblacion[self.posicion_mejor_individuo]
        tam_aux_Pop-=1
        #self.poblacion[tam_aux_Pop]=mejor_individuo
    #print("MEJOR INDIVIDUO EN: " + str(self.posicion_mejor_individuo))
    #print("IND")
    #print(mejor_indiviudo)
    #print("finIND")
    if(len(individuos)<tam_aux_Pop-1):
        while(j<len(individuos) and j<tam_aux_Pop-1 and i<tam_aux_Pop-1):
            if(i!=self.posicion_mejor_individuo):
                self.poblacion[i]=individuos[j]
                j+=1
            i+=1
        j=0

    #Se verifica Elitismo
    if(self.posicion_mejor_individuo!=-1):
        self.posicion_mejor_individuo= self.poblacion.index(min(self.poblacion))

def reemplazo_mu_mas_lambda(self, individuos):
    """
    Padres e Hijos son agrupados y ordenados de acuerdo al fitness.
    De ese orden se eligen los n mejores, que compondrán a la nueva
    población
    --------------------
    @Param self: referencia al objeto GA Real
    @Param individuos: array que contiene a los hijos obtenidos del cruzamiento entre padres
    --------------------
    @Return none
    """
    individuos= [(self.adaptacion(individuo), individuo)
                for individuo in individuos]

    aux_Pop= self.poblacion + individuos
    temp_Pop=[]
    tam_aux_Pop= self.tam_poblacion

    #Suma de Aptitudes de la Nueva Población
    suma_aptitudes=1.0*sum(p[0] for p in aux_Pop)
    #Fitness Relativo de cada individuo en aux_Pop
    fitness_relativo=[(p[0]/suma_aptitudes) for p in aux_Pop]

    #Se verifica si se hace uso de Elitismo
    if(self.posicion_mejor_individuo!=-1):
        mejor_individuo= self.poblacion[self.posicion_mejor_individuo]
        tam_aux_Pop-=1
        self.poblacion[tam_aux_Pop]=mejor_individuo

    #Se verifica que operador de seleccion se usa para el reemplazo
    if(self.op_seleccion_reemplazo==0):
        #Reemplazo usando Selección por Ruleta
        for i in range(tam_aux_Pop-1):
            pos_ind= seleccion_Ruleta(aux_Pop, fitness_relativo)
            self.poblacion[i]= aux_Pop[pos_ind]

    if(self.op_seleccion_reemplazo==1):
        #Reemplazo usando Selección SUS
        pos_ind= seleccion_SUS(aux_Pop, tam_aux_Pop-1, fitness_relativo)
        i=0
        for p_ind in pos_ind:
            self.poblacion[i]= aux_Pop[p_ind]
            i+=1

    if(self.op_seleccion_reemplazo==2):
        #Reemplazo usando Selección por Torneo Binario
        for i in range(tam_aux_Pop-1):
            pos_ind= seleccion_Torneo_Binario(aux_Pop, len(aux_Pop))
            self.poblacion[i]=aux_Pop[pos_ind]

    if(self.op_seleccion_reemplazo==3):
        #Seleccion para el reemplazo mu mejores, sacar metodo aparte
        for i in range(tam_aux_Pop - 1):
            pos_ind = aux_Pop.index(max(aux_Pop))
            self.poblacion[i] = aux_Pop.pop(pos_ind)

    #Se verifica Elitismo
    if(self.posicion_mejor_individuo!=-1):
        self.posicion_mejor_individuo= self.poblacion.index(min(self.poblacion))

def reemplazo_mu_lambda(self,individuos):
    """
    Consiste en hacer un reemplazo de la población actual por
    los hijos que se han obtenido. Es similar al reemplazo generacional,
    salvo que en este caso, la cantidad de hijos obtenidos es mayor al
    tamaño de la población de individuos que se utiliza
    --------------------
    @Param self: referencia al objeto GA Real
    @Param individuos: array que contiene a los hijos obtenidos del cruzamiento entre padres
    --------------------
    @Return none
    """
    #Calculo el fitness de los hijos obtenidos
    individuos= [(self.adaptacion(individuo), individuo)
                 for individuo in individuos]
    tam_aux_Pop= self.tam_poblacion

    #Suma de aptitudes de los individuos
    suma_aptitudes=1.0*sum(ind[0] for ind in individuos)
    #Fitness Relativo de los individuos
    fitness_relativo=[(ind[0]/suma_aptitudes) for ind in individuos]

    #Se verifica si se hace uso de Elitismo
    if(self.posicion_mejor_individuo!=-1):
        mejor_individuo= self.poblacion[self.posicion_mejor_individuo]
        tam_aux_Pop-=1
        self.poblacion[tam_aux_Pop]=mejor_individuo

    #Se verifica que operador de seleccion se usa para el reemplazo
    if(self.op_seleccion_reemplazo==0):
        #Reemplazo usando Selección por Ruleta
        if(len(individuos)>0):
            for i in range(tam_aux_Pop-1):
                pos_ind= seleccion_Ruleta(individuos, fitness_relativo)
                self.poblacion[i]= individuos[pos_ind]

    if(self.op_seleccion_reemplazo==1):
        #Reemplazo usando Selección SUS
        if(len(individuos)):
            pos_ind= seleccion_SUS(individuos, tam_aux_Pop-1, fitness_relativo)
            i=0
            for p_ind in pos_ind:
                self.poblacion[i]= individuos[p_ind]
                i+=1

    if(self.op_seleccion_reemplazo==2):
        #Reemplazo usando Selección por Torneo Binario
        if(len(individuos)):
            for i in range(tam_aux_Pop-1):
                pos_ind= seleccion_Torneo_Binario(individuos, len(individuos))
                self.poblacion[i]=individuos[pos_ind]

    if(self.op_seleccion_reemplazo==3):
        #Seleccion para el reemplazo mu mejores, sacar metodo aparte
        for i in range(tam_aux_Pop - 1):
            pos_ind = individuos.index(max(individuos))
            self.poblacion[i] = individuos.pop(pos_ind)

    #Se verifica Elitismo
    if(self.posicion_mejor_individuo!=-1):
        self.posicion_mejor_individuo= self.poblacion.index(min(self.poblacion))

"""
Métodos Auxiliares que son usados por SUS
"""
def makeWheel(population, relative_fitness):
    wheel = []
    #total = sum(p[0] for p in population)
    top = 0
    pos=0
    for p in range(len(population)):
        f = relative_fitness[p]
        wheel.append((top, top+f, population[p], pos))
        top += f
        pos+=1

    return wheel

def binSearch(wheel, num):
    mid = len(wheel)//2
    low, high, answer, pos = wheel[mid]
    if low<=num<=high:
        return pos
    elif high < num:
        return binSearch(wheel[mid+1:], num)
    else:
        return binSearch(wheel[:mid], num)
