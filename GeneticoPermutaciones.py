﻿import numpy as np
import sys
sys.path.append(sys.path[0]+'\\Permutacion')
import P_operadores
import P_metodosUtiles
import abstractClass
from time import time
import random
import json

class ResultadosFSSPPermutacion():
    def __init__(self):
        abstractClass.Resultados.__init__(self)

class ResultadosFSSPPermutacionEncoder(json.JSONEncoder):
 
    def default(self, obj):
        return obj.__dict__

class ProblemaFSSPPermutacion:
    """
    Clase de ProblemaFSSPPermutacion
    """
    def __init__(self, no_of_jobs, no_of_machines, processing_time):

        self.no_of_jobs = no_of_jobs
        self.no_of_machines = no_of_machines
        self.processing_time = processing_time

    def estado_aleatorio(self):
        return tuple(np.random.permutation(self.no_of_jobs))

class GeneticoPermutaciones():

    def __init__(self, problema, n_poblacion, prob_muta, prob_cruce, nro_padres, tipo_operador_cruce, tipo_operador_seleccion,tipo_operador_mutacion,tipo_operador_reemplazo, tipo_operador_seleccion_para_reemplazo,elitismo,lambda_cantidad_de_hijos, optimo ):
        """
        @param prob_muta : Probabilidad de mutación de un cromosoma
                           (0.01 por defualt)

        """
        #Genetico.__init__(self, problema, n_poblacion)
        self.cantidad_evaluaciones_funcion_fitness = 0
        self.problema = problema
        self.prob_muta = prob_muta
        self.prob_cruce = prob_cruce
        self.nro_padres = nro_padres
        self.lambda_cantidad_de_hijos = lambda_cantidad_de_hijos
        self.tipo_operador_cruce = tipo_operador_cruce
        self.tipo_operador_seleccion = tipo_operador_seleccion
        self.tipo_operador_mutacion = tipo_operador_mutacion
        self.tipo_operador_reemplazo = tipo_operador_reemplazo
        self.tipo_operador_seleccion_para_reemplazo = tipo_operador_seleccion_para_reemplazo 
        self.n_poblacion = n_poblacion
        self.optimo = optimo
        self.inicializa_poblacion(n_poblacion, elitismo);

        """super().__init__(problema, n_población)"""

    def inicializa_poblacion(self, n_poblacion, elitismo):

        individuos = [self.estado_a_cadena(self.problema.estado_aleatorio())
                      for _ in range(self.n_poblacion)]
        self.poblacion = [(self.adaptacion(individuo), individuo)
                          for individuo in individuos]

        if(elitismo):
            self.posicion_mejor_individuo = self.poblacion.index(min(self.poblacion))
            #print("MEJOR INDIVIDUO EN Inicial: " + str(self.posicion_mejor_individuo))
        else:
            self.posicion_mejor_individuo = -1
        #print("Y el conjunto de estados iniciales es: ")
        #for (ind, (aptitud, individuo)) in enumerate(self.poblacion):
        #    assert isinstance(individuo, list)
        #    print('{}: {} con {} de aptitud'.format(ind, individuo, aptitud))

        #print("--------------")
        #print("cantidad: " + str(self.n_poblacion))
        #print("cantidad: " + str(len(self.poblacion)))
        #print("numero de padres: " + str(self.nro_padres))
        #print("--------------")
        #print("mejor valor")
        #print(min(self.poblacion))
        #print("mejor valor")
        #print(max(self.poblacion))
        #print("----------------------")
        #print(self.poblacion)


    def adaptacion(self,individuo):
        self.cantidad_evaluaciones_funcion_fitness +=1
        # list for the time passed until the finishing of the job
        cost = [0] * self.problema.no_of_jobs
        # for each machine, total time passed will be updated
        for machine_no in range(0, self.problema.no_of_machines):
            for slot in range(self.problema.no_of_jobs):
                # time passed so far until the task starts to process
                cost_so_far = cost[slot]
                if slot > 0:
                    cost_so_far = max(cost[slot - 1], cost[slot])
                cost[slot] = cost_so_far + self.problema.processing_time[individuo[slot]][machine_no]
        return cost[self.problema.no_of_jobs - 1]

    @staticmethod
    def estado_a_cadena(estado):
        """
        Convierte un estado a una cadena de cromosomas

        @param estado: Una tupla con un estado
        @return: Una lista con una cadena de caracteres

        Por default converte el estado en una lista.

        """
        return list(estado)



    def busqueda(self, n_generaciones):
        resultados = ResultadosFSSPPermutacion()
        ban=1
        tiempo_inicial = time()
        tiempo_mejor_solucion = time()
        mejor_solucion = (min(self.poblacion)[0])
        peor_solucion = (max(self.poblacion)[0])
        generacion_mejor_solucion=0
        print("--------------------------------")
        print("--------------------------------")
        print("Mejor de poblacion inicial:")
        print(min(self.poblacion)[0])
        print("Peor de poblacion inicial:")
        print(max(self.poblacion)[0]) 
        print("Promedio:")
        suma = 0
        for ind in self.poblacion:
            suma = suma + ind[0]
        avg = suma / self.n_poblacion 
        print(avg) 
        print("--------------------------------")
        print("--------------------------------")
        resultados.mejor_solucion_inicial = mejor_solucion
        resultados.peor_solucion_inicial = peor_solucion
        resultados.promedio_solucion_inicial = avg

        for i in range(n_generaciones):

            if (mejor_solucion > min(self.poblacion)[0]):
                mejor_solucion = min(self.poblacion)[0]
                tiempo_mejor_solucion = time()
                generacion_mejor_solucion = i

            indices_parejas = self.seleccion()
            hijos = self.cruza(indices_parejas)
            self.mutacion(hijos)
            self.reemplazo(hijos)

        """for (ind, (aptitud, individuo)) in enumerate(self.poblacion):
            assert isinstance(individuo, list)
            print('{}: {} con {} de aptitud'.format(ind, individuo, aptitud))"""
        tiempo_final = time()
        print("\n")
        print("\n")
        print("------Tiempo total----------")
        print(tiempo_final - tiempo_inicial)
        resultados.tiempo_total_ejecucion = tiempo_final - tiempo_inicial
        print("------Fitness mejor solucion----------")
        print(mejor_solucion)
        resultados.mejor_solucion  = mejor_solucion
        print("En generacion : " + str(generacion_mejor_solucion))
        resultados.generacion_mejor_solucion = generacion_mejor_solucion
        print("------Tiempo mejor solucion----------")
        print(tiempo_mejor_solucion - tiempo_inicial)
        resultados.tiempo_mejor_solucion = tiempo_mejor_solucion - tiempo_inicial
        print("------Cantidad de evaliaciones de funcion de fintess ------------")
        print(self.cantidad_evaluaciones_funcion_fitness)
        resultados.cantidad_evaluaciones_funcion_fitness = self.cantidad_evaluaciones_funcion_fitness
        print("------Total de generaciones ------------")
        print(n_generaciones)
        resultados.total_generaciones  = n_generaciones
        print("-----------------------:")
        print("\n")
        print("--------------------------------")
        print("--------------------------------")
        print("Mejor de poblacion final:")
        print(max(self.poblacion)[0])
        resultados.mejor_solucion_final = max(self.poblacion)[0]
        print("Peor de poblacion final:")
        resultados.peor_solucion_final = min(self.poblacion)[0]
        print(min(self.poblacion)[0]) 
        print("Promedio:")
        suma = 0
        for ind in self.poblacion:
            suma = suma + ind[0]
        avg = suma / self.n_poblacion 
        resultados.promedio_solucion_final = avg
        print(avg) 
        print("diferencia relativa con optimo")
        resultados.diferencia_relativa_optimo = abs(self.optimo-mejor_solucion) / abs(self.optimo)
        resultados.error_relativo = (abs(self.optimo-mejor_solucion) / abs(self.optimo))*100
        print("error relativo")

        resultados.optimo = 1

        print("--------------------------------")
        print("--------------------------------")
        print(resultados.__dict__)
        return (resultados.__dict__)
    
    def seleccion(self):

        padres=[]
        #Suma de Aptitudes de la población actual
        suma_aptitudes=1.0*sum(pop[0] for pop in self.poblacion)
        #Fitness relativo de los individuos en base a la suma de aptitudes anterior
        fitness_relativo=[(abs(pop[0]/suma_aptitudes)) for pop in self.poblacion]


        if (self.tipo_operador_seleccion == 0):
            padres = [P_operadores.seleccion_torneo_binario(self.poblacion, self.n_poblacion)
                    for _ in range(self.nro_padres)]
            self.indice_padres = padres 
            return (P_metodosUtiles.armar_parejas(padres))

        if (self.tipo_operador_seleccion == 1):
            padres = [P_operadores.seleccion_ruleta(self.poblacion,fitness_relativo)
                    for _ in range(self.nro_padres)]
            self.indice_padres = padres 
            return (P_metodosUtiles.armar_parejas(padres))

        if (self.tipo_operador_seleccion == 2):
            return (P_metodosUtiles.armar_parejas(P_operadores.seleccion_SUS(self.poblacion,self.nro_padres,fitness_relativo)))

    def cruza(self, ind_parejas):

        if self.tipo_operador_cruce == 0:
            hijos = []
            #primer operador
            for (i,j) in ind_parejas:
                #Recorro todas las parejas
                if random.uniform(0,1) < self.prob_cruce:
                    #si es menor a prob_cruce, entonces hago el cruce con los padres correspondientes para generar, en este caso, un hijo.
                    par_de_hijos = P_operadores.cruza_PMX(self,self.poblacion[i][1],self.poblacion[j][1])
                    hijos.append(par_de_hijos[0])
                    hijos.append(par_de_hijos[1])
                else:
                    #sino, agrego directamente ambos padres.
                    hijos.append(self.poblacion[i][1])
                    hijos.append(self.poblacion[j][1])
                
                if(len(hijos) == self.lambda_cantidad_de_hijos):
                    #Aca verifico que no se vaya de tamaño el vector de hijos, por lo tanto, si se pasa borro el ultimo elemento y termino el for.
                    break
                elif (len(hijos) > self.lambda_cantidad_de_hijos):
                    print("NODEBERIAENTRARACA")
                    hijos.pop()
                    break
            return hijos


        if self.tipo_operador_cruce == 1:
            hijos = []
             #primer operador
            for (i,j) in ind_parejas:
                #Recorro todas las parejas
                if random.uniform(0,1) < self.prob_cruce:
                    #si es menor a prob_cruce, entonces hago el cruce con los padres correspondientes para generar, en este caso, un hijo.
                    hijos.append(P_operadores.cruza_OX(self,self.poblacion[i][1],
                                          self.poblacion[j][1]))
                else:
                    #sino, agrego directamente ambos padres.
                    hijos.append(self.poblacion[i][1])
                    hijos.append(self.poblacion[j][1])
                
                if(len(hijos) == self.lambda_cantidad_de_hijos):
                    #Aca verifico que no se vaya de tamaño el vector de hijos, por lo tanto, si se pasa borro el ultimo elemento y termino el for.
                    break
                elif (len(hijos) > self.lambda_cantidad_de_hijos):
                    hijos.pop()
                    break
            return hijos

        if self.tipo_operador_cruce == 2:

            hijos = []
            #primer operador
            for (i,j) in ind_parejas:
                #Recorro todas las parejas
                if random.uniform(0,1) < self.prob_cruce:
                    #si es menor a prob_cruce, entonces hago el cruce con los padres correspondientes para generar, en este caso, un hijo.
                    hijos.append(P_operadores.cruza_CX(self,self.poblacion[i][1],
                                          self.poblacion[j][1]))
                else:
                    #sino, agrego directamente ambos padres.
                    hijos.append(self.poblacion[i][1])
                    hijos.append(self.poblacion[j][1])
                
                if(len(hijos) == self.lambda_cantidad_de_hijos):
                    #Aca verifico que no se vaya de tamaño el vector de hijos, por lo tanto, si se pasa borro el ultimo elemento y termino el for.
                    break
                elif (len(hijos) > self.lambda_cantidad_de_hijos):
                    hijos.pop()
                    break
            return hijos

    def mutacion(self, individuos):

        if (self.tipo_operador_mutacion == 0):
            for individuo in individuos:
                P_operadores.mutacion_intercambio_reciproco(self,individuo)

        if (self.tipo_operador_mutacion == 1):
            for individuo in individuos:
                P_operadores.mutacion_inversion(self,individuo)   
               
        if (self.tipo_operador_mutacion == 2):
            for individuo in individuos:
                P_operadores.mutacion_insercion(self,individuo)   

        if (self.tipo_operador_mutacion == 3):
            for individuo in individuos:
                P_operadores.mutacion_desplazamiento(self,individuo)   


    def reemplazo(self,individuos):

        if (self.tipo_operador_reemplazo == 0):
            P_operadores.reemplazo_mu_mas_lambda(self,individuos)

        if (self.tipo_operador_reemplazo == 1):
            P_operadores.reemplazo_mu_lambda(self,individuos)

        if (self.tipo_operador_reemplazo == 2):
            P_operadores.reemplazo_generacional(self,individuos)