#Testing Unitario Genetico Real
import unittest
import geneticoReal_test

class RTestModule(unittest.TestCase):
    padre1=[1.2,3.5,-0.75,-9.24,36.48,52.01,-68.14]
    padre2=[9.4,0.65,-8.21,-12.47,30.45,-72.2,85.21]
    #resultados Cruce aritmetico, correcto e incorrecto
    resAritmeticoOk=[5.3,2.075,-4.48,-10.855,33.465,-10.095,8.535]
    resAritmeticoMal=[1,2,3,4,5,6,7]
    def testCruzamientoAritmetico(self):
        self.assertEqual(geneticoReal_test.cruzamientoAritmetico([1.2,3.5,-0.75,-9.24,36.48,52.01,-68.14], [9.4,0.65,-8.21,-12.47,30.45,-72.2,85.21]),[5.3,2.075,-4.48,-10.855,33.465,-10.095,8.535], "Todo OK!")
    def testCruzamientoAritmetico1(self):
        self.assertEqual(geneticoReal_test.cruzamientoAritmetico([1.2,3.5,-0.75,-9.24,36.48,52.01,-68.14], [9.4,0.65,-8.21,-12.47,30.45,-72.2,85.21]),[1,2,3,4,5,6,7],"Fallo de Test")

if __name__=="__main__":
    unittest.main()




