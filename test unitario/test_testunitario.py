#!/usr/bin/env python
# -*- coding: utf-8 -*-
import unittest
import testunitario
class TestMyModule(unittest.TestCase):
    
    def test_cruza_uniform(self):
        self.assertEqual(testunitario.cruza_uniform([1,1,1,1,1,1,1,1,1,1,1], [0,0,0,0,0,0,0,0,0,0,0],[0,1,1,0,1,1,1,1,0,1,1]), [0,1,1,0,1,1,1,1,0,1,1])
    def test_cruza_uniform1(self):
        self.assertEqual(testunitario.cruza_uniform([1,1,1,1,1,1,1,1,1,1,1], [0,0,0,0,0,0,0,0,0,0,0],[0,1,1,0,1,1,1,1,0,1,1]), [0,1,1,0,1,1,1,1,0,1,1])

if __name__ == "__main__":
    unittest.main()