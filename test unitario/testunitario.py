
def cruza_uniform(cadena1,cadena2,mask):
    #mask = [random.randint(0,1) for _ in range(len(cadena1))]
    hijo = cadena1[:]
    for i in range(len(mask)-1):
        if (mask[i] == 1):
            hijo[i] = cadena1[i]
        else: 
            hijo[i] = cadena2[i]
    return hijo