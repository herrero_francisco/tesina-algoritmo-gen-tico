import random
import json
import sys


class Resultados: 
    def __init__(self):
        self.mejor_solucion_inicial = -1
        self.peor_solucion_inicial = -1
        self.promedio_solucion_inicial = -1
        self.mejor_solucion_final = -1 
        self.peor_solucion_final = -1
        self.promedio_solucion_final = -1
        self.tiempo_total_ejecucion = -1
        self.tiempo_mejor_solucion = -1
        self.cantidad_evaluaciones_funcion_fitness = -1
        self.diferencia_relativa_optimo = -1
        self.error_relativo = -1
        self.total_generaciones = -1
        self.generacion_mejor_solucion = -1
        self.mejor_solucion = -1
        self.optimo=-1




class Genetico:
    def __init__(self, problema, n_poblacion, prob_muta=0.01):
        self.problema = problema
        self.probMutacion=prob_muta

    def inicializarPoblacion(self):
        raise NotImplementedError("Falta desarrollar el metodo")
    def ruleta(self):
        raise NotImplementedError("Falta desarrollar el método")
    def sus(self):
        raise NotImplementedError("Falta desarrollar el método")
    def torneo(self):
        raise NotImplementedError("Falta desarrollar el método")
    def ejecutar(self):
        raise NotImplementedError("Falta desarrollar el método")
    
class Problema:
    def estado_aleatorio(self):
        """
        Devuelve un estado aleatorio con distribución uniforme
        sobre el espacio de estados.

        @return: Una tupla que representa un estado

        """
        raise NotImplementedError("Falta desarrollar el método")

    def fitness(self, estado):
        """
        Calcula el costo de un estado

        @param estado: Una tupla con un estado válido en el espacio de estados

        @return: Un número (flotante o entero) con el costo del estado
        """
        raise NotImplementedError("Falta desarrollar el método")
