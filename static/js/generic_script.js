/*
 * Script Generico. Contiene metodos que utilizan los tres algoritmos de igual forma y que no cambia
 * su logica
 */
//Alerta de Informacion
function infoAlert(msg) {
    Swal.fire({
        title: '<h3>Atencion!</h3>',
        icon: 'info',
        html: '<h5>' + msg + '</h5>',
        showCloseButton: true,
        focusConfirm: false,
        confirmButtonText:
            '<h4>Ok</h4>',
        confirmButtonAriaLabel: 'Genial!'
    });
}
//Alerta de Error
function errorAlert(msg) {
    Swal.fire({
        title: '<h3>Error!</h3>',
        icon: 'error',
        html: '<h5>' + msg + '</h5>',
        showCloseButton: true,
        focusConfirm: false,
        confirmButtonText:
            '<h4>Ok</h4>',
        confirmButtonAriaLabel: 'Genial!'
    });
}
//Comparar Strings
function compareStrings(str1, str2){
    var reA = /[^a-zA-Z]/g;
    var reN = /[^0-9]/g;
    //(_+)|([0-9]+)|([^0-9_]+)
    var aA = str1.toLowerCase().replace(reA, "");
    var bA = str2.toLowerCase().replace(reA, "");
    if (aA === bA) {
        var aN = parseInt(str1.toLowerCase().replace(reN, ""), 10);
        var bN = parseInt(str2.toLowerCase().replace(reN, ""), 10);
        return aN === bN ? 0 : aN > bN ? 1 : -1;
    } else {
        return aA > bA ? 1 : -1;
    }    
}