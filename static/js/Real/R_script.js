//Controla que el archivo contenga todos los parametros necesarios
var inferiores = [-100, -100, -100, -5, -600, -32];
var superiores = [100, 100, 100, 5, 600, 32];
var rProblems=["optimizacionFunciones"];// La idea es que esto se vaya cargando con los distintos problemas que se agreguen
function rCheckFile(archivo, callback) {
    var reader = new FileReader();
    reader.onload = function (e) {
        var lines = this.result.split("\n");
        console.log(lines);
        if (lines.length == 16) {
            return callback(lines);
        } else {
            return callback(null);
        }
    };
    reader.readAsText(archivo);
}
//Control de Parametros que se encuentran en el archivo de configuracion
function rCheckConfigValues(params) {
    /*NOTA:
     * En principio se controla la configuracion del Algoritmo Genetico.
     * Si esta todo bien, se realiza el chequeo de los parametros de configuracion
     * del problema en particular, es por eso que en el archivo se especifica el tipo de
     * problema
     */
    var rParams = params.slice(0, 13);
    var probParams = params.slice(13);
    var formData = new FormData();
    var data = [];
    for (var i = 0; i < rParams.length; i++) {
        data[i] = (rParams[i].split("//"))[0];
    }
    //Control de Operadores y Probabilidades
    var msg = "<ul>";
    if (parseInt(data[0], 10) < 1)
        msg += "<li>La poblacion Inicial NO puede ser menor o igual que 0</li>";
    if (parseInt(data[1], 10) < 1) {
        msg += "<li>La poblacion de hijos (lambda) NO puede ser negativa, ni tampoco 0</li>";
    } else {
        if (parseInt(data[1],10) > parseInt(data[0],10))
            msg += "<li>La poblacion de hijos NO puede ser mayor que la poblacion de padres</li>";
    }
    if (parseInt(data[2], 10) < 1)
        msg += "<li>El Algoritmo Genetico debe correr, al menos, una Generacion</li>";
    if (parseInt(data[3], 10) < 1)
        msg += "<li>El Algoritmo Genetico debe realizar, al menos, una ejecucion o corrida</li>";
    if (parseInt(data[4], 10) < 0 || parseInt(data[4],10) > 2)
        msg += "<li>El Operador de Seleccion no es correcto</li>";
    if (parseInt(data[5],10) < 0 || parseInt(data[5],10) > 3)
        msg + "<li>El Operador de Cruce no es correcto</li>";
    if (parseFloat(data[6]) <=0 || parseFloat(data[6]) >=1)
        msg += "<li>El Valor de alfa debe ser ente 0,0001 y 0,9999</li>";
    if (parseFloat(data[7]) <= 0 || parseFloat(data[7]) >= 1)
        msg += "<li>La probabilidad de Cruce NO puede ser negativa, tampoco puede ser mayor a 1</li>";
    if (parseInt(data[8],10) < 0 || parseInt(data[8],10) > 1)
        msg += "<li>El Operador de Mutacion no es correcto</li>";
    if (parseFloat(data[9]) <= 0 || parseFloat(data[9]) >= 1)
        msg += "<li>La probabilidad de Mutación NO puede ser negativa, tampoco puede ser mayor a 1</li>";
    if (parseInt(data[10], 10) < 0 || parseInt(data[10], 10) > 2)
        msg += "<li>La Estrategia de Reemplazo no es correcta</li>";
    if (parseInt(data[11], 10) < 0 || parseInt(data[11], 10) > 3)
        msg += "<li>El Operador de Seleccion para Reemplazo no es correcto</li>";
    if (parseInt(data[12], 10) < 0 || parseInt(data[12], 10) > 1)
        msg += "<li>Elitismo solo toma dos valores 0 (NO) o 1 (SI)</li>";
    msg += "</ul>";
    if (msg != "<ul></ul>") {
        Swal.fire({
            title: '<h3>Error!</h3>',
            icon: 'error',
            html: msg,
            showCloseButton: true,
            focusConfirm: false,
            confirmButtonText:
                '<h4>Ok</h4>',
            confirmButtonAriaLabel: 'Ok'
        });
    } else {
        var probMsg = "<ul>";
        var probData = [];
        for (var i = 0; i < probParams.length; i++) {
            probData[i] = (probParams[i].split("//"))[0];
        }
        switch (probData[0]) {
            case '0':
                probMsg += pof_checkProblemConfig(probData);
                probMsg += "</ul>";
                if (probMsg != "<ul></ul>") {
                    Swal.fire({
                        title: '<h3>Error!</h3>',
                        icon: 'error',
                        html: probMsg,
                        showCloseButton: true,
                        focusConfirm: false,
                        confirmButtonText:
                            '<h4>Ok</h4>',
                        confirmButtonAriaLabel: 'Ok'
                    });
                } else {
                    //Append de los valores de parametros al formData y retorno
                    formData.append("poblacion_inicial", data[0]);
                    formData.append("lambda_cantidad_de_hijos", data[1]);
                    formData.append("cantidad_generaciones", data[2]);
                    formData.append("cantidad_ejecuciones", data[3]);
                    formData.append("selectSeleccion", data[4]);
                    formData.append("selectCruce", data[5]);
                    formData.append("valorAlfa", data[6]);
                    formData.append("probabilidad_cruce", data[7]);
                    formData.append("selectMutacion", data[8]);
                    formData.append("probabilidad_mutacion", data[9]);
                    formData.append("selectReemplazo", data[10]);
                    formData.append("selectSeleccionReemplazo", data[11]);
                    formData.append("chbxElitismo", data[12]);
                    formData.append("problema", rProblems[(probData[0])]);
                    formData.append("funciones", probData[1]);
                    formData.append("limiteInferior", inferiores[((probData[1])-1)]);
                    formData.append("limiteSuperior", superiores[((probData[1])-1)]);
                    formData.append("tam_dimension", probData[2]);
                    return formData;
                }
                break;
            default:
                Swal.fire({
                    title: '<h3>Error!</h3>',
                    icon: 'error',
                    html: '<h5>La opcion de problema no es correcta</h5>',
                    showCloseButton: true,
                    focusConfirm: false,
                    confirmButtonText:
                        '<h4>Ok</h4>',
                    confirmButtonAriaLabel: 'Ok'
                });
                return null;
                break;
        }
    }
}