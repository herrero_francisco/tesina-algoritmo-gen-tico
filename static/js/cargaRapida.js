//Script de Control Modal de Carga Rapida
$("#selectRepresentacion").change(function () {
    var opt = $(this).children("option:selected").val();
    changeListValues(opt);
});
function changeListValues(opt) {
    if (opt == "0") {
        $("#repBinaria").show();
        $("#repBinaria1").show();
        $("#repBinaria2").show();
        $("#repBinaria3").show();
        $("#repReal").hide();
        $("#repReal1").hide();
        $("#repReal2").hide();
        $("#repPermutacion").hide();
        $("#repPermutacion1").hide();
        $("#repPermutacion2").hide();
        $("#repPermutacion3").hide();
        $("#puntos").show();
        $("#alfa").hide();
        $("#funcion").hide();
        $("#tam_elemento").hide();
        $("#funciones").hide();
    }
    if (opt == "1") {
        $("#repReal").show();
        $("#repReal1").show();
        $("#repReal2").show();
        $("#repPermutacion").hide();
        $("#repPermutacion1").hide();
        $("#repPermutacion2").hide();
        $("#repPermutacion3").hide();
        $("#repBinaria").hide();
        $("#repBinaria1").hide();
        $("#repBinaria2").hide();
        $("#repBinaria3").hide();
        $("#puntos").hide();
        $("#alfa").show();
        $("#funcion").show();
        $("#tam_elemento").show();
        $("#funciones").show();
    }
    if (opt == "2") {
        $("#repPermutacion").show();
        $("#repPermutacion1").show();
        $("#repPermutacion2").show();
        $("#repPermutacion3").show();
        $("#repReal").hide();
        $("#repReal1").hide();
        $("#repReal2").hide();
        $("#repBinaria").hide();
        $("#repBinaria1").hide();
        $("#repBinaria2").hide();
        $("#puntos").hide();
        $("#alfa").hide();
        $("#funcion").hide();
        $("#tam_elemento").hide();
        $("#funciones").hide();
    }
}