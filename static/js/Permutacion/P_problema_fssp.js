function P_problema_fssp_checkValues() {
    var elegir_instancia = $("#elegirInstancia").prop("checked");

    var msg = "<ul>";

    if (elegir_instancia) {
        var seleccionar_instancia = $("#selectInstancia").val();
        if (seleccionar_instancia == null) {
            msg += "<li>Se debe elegir una instancia</li>";

        }
    }
    else {
        var archivo_instancia = $("#archivoInstancia").val();
        if (archivo_instancia == "") {
            msg += "<li>Se debe agregar una archivo de instancia</li>";

        }
    }
    msg += "</ul>";
    return msg;
}
//Control del Archivo de Configuración del Problema
function fssp_checkProblemConfig(params){
    var res="";
    existsInstance(params[1], function(result){
        if(!result){
            res+="<li>El archivo de instancia NO existe</li>";
        }
    })
    return res;
}
//Ver si existe el archivo instancia
function existsInstance(filename, callback){
    var res=false;
    $.ajax({
        url:'/utils/instancias_fssp_permutaciones',
        success:function(res){
            var archivos=res["dir"];
            return callback(archivos.includes(filename));
        },
        error:function(){
            console.error("No se ha podido obtener la información");
            return callback(false);
        }
    });
    return res;
}