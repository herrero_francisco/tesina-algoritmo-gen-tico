var pProblems=["fssp"];
//Script de Control General para Algoritmo Genetico Permutacion
function pCheckFile(archivo, callback) {
    var reader = new FileReader();
    reader.onload = function (e) {
        var lines = this.result.split("\n");
        if (lines.length == 14) {
            return callback(lines);
        } else {
            return callback(null);
        }
    };
    reader.readAsText(archivo);
}
//Control de Parametros del Archivo de Configuracion
function pCheckConfigValues(params){
    /*NOTA:
     * En principio se controla la configuracion del Algoritmo Genetico.
     * Si esta todo bien, se realiza el chequeo de los parametros de configuracion
     * del problema en particular, es por eso que en el archivo se especifica el tipo de
     * problema
     */
    var rParams = params.slice(0, 12);
    var probParams = params.slice(12);
    var formData = new FormData();
    var data = [];
    for (var i = 0; i < rParams.length; i++) {
        data[i] = (rParams[i].split("//"))[0];
    }
    //Control de Operadores y Probabilidades
    var msg = "<ul>";
    if(parseInt(data[0],10)<1){
        msg += "<li>La poblacion Inicial NO puede ser menor o igual que 0</li>";
    }
    if(parseInt(data[1],0)<1){
        msg += "<li>La poblacion de hijos (lambda) NO puede ser negativa, ni tampoco 0</li>";
    }else{
        if(parseInt(data[1],10)>parseInt(data[0],10)){
            msg += "<li>La poblacion de hijos NO puede ser mayor que la poblacion de padres</li>";
        }
    }
    if (parseInt(data[2], 10) < 1)
        msg += "<li>El Algoritmo Genetico debe correr, al menos, una Generacion</li>";
    if (parseInt(data[3], 10) < 1)
        msg += "<li>El Algoritmo Genetico debe realizar, al menos, una ejecucion o corrida</li>";
    if (parseInt(data[4], 10) < 0 || parseInt(data[4],10) > 2)
        msg += "<li>El Operador de Seleccion no es correcto</li>";
    if (parseInt(data[5],10) < 0 || parseInt(data[5],10) > 2)
        msg + "<li>El Operador de Cruce no es correcto</li>";
    if (parseFloat(data[6]) <= 0 || parseFloat(data[6]) >= 1)
        msg += "<li>La probabilidad de Cruce NO puede ser negativa, tampoco puede ser mayor a 1</li>";
    if (parseInt(data[7],10)<0 || parseInt(data[7],10)>3)
        msg += "<li>El Operador de Mutacion no es correcto</li>";
    if (parseFloat(data[8]) <= 0 || parseFloat(data[8]) >= 1)
        msg += "<li>La probabilidad de Mutación NO puede ser negativa, tampoco puede ser mayor a 1</li>";
    if (parseInt(data[9], 10) < 0 || parseInt(data[9], 10) > 2)
        msg += "<li>La Estrategia de Reemplazo no es correcta</li>";
    if (parseInt(data[10], 10) < 0 || parseInt(data[10], 10) > 3)
        msg += "<li>El Operador de Seleccion para Reemplazo no es correcto</li>";
    if (parseInt(data[11], 10) < 0 || parseInt(data[11], 10) > 1)
        msg += "<li>Elitismo solo toma dos valores 0 (NO) o 1 (SI)</li>";
    msg += "</ul>";
    if (msg != "<ul></ul>") {
        Swal.fire({
            title: '<h3>Error!</h3>',
            icon: 'error',
            html: msg,
            showCloseButton: true,
            focusConfirm: false,
            confirmButtonText:
                '<h4>Ok</h4>',
            confirmButtonAriaLabel: 'Ok'
        });
    }else{
        var probMsg = "<ul>";
        var probData = [];
        for (var i = 0; i < probParams.length; i++) {
            probData[i] = (probParams[i].split("//"))[0];
        }
        switch (probData[0]) {
            case '0':
                probMsg += fssp_checkProblemConfig(probData);
                probMsg += "</ul>";
                if (probMsg != "<ul></ul>") {
                    Swal.fire({
                        title: '<h3>Error!</h3>',
                        icon: 'error',
                        html: probMsg,
                        showCloseButton: true,
                        focusConfirm: false,
                        confirmButtonText:
                            '<h4>Ok</h4>',
                        confirmButtonAriaLabel: 'Ok'
                    });
                } else {
                    //Append de los valores de parametros al formData y retorno
                    formData.append("poblacion_inicial", data[0]);
                    formData.append("lambda_cantidad_de_hijos", data[1]);
                    formData.append("cantidad_generaciones", data[2]);
                    formData.append("cantidad_ejecuciones", data[3]);
                    formData.append("selectSeleccion", data[4]);
                    formData.append("selectCruce", data[5]);
                    formData.append("probabilidad_cruce", data[6]);
                    formData.append("selectMutacion", data[7]);
                    formData.append("probabilidad_mutacion", data[8]);
                    formData.append("selectReemplazo", data[9]);
                    formData.append("selectSeleccionReemplazo", data[10]);
                    formData.append("chbxElitismo", data[11]);
                    formData.append("problema", pProblems[(probData[0])]);
                    formData.append("selectInstancia", probData[1]);
                    return formData;
                }
                break;
            default:
                Swal.fire({
                    title: '<h3>Error!</h3>',
                    icon: 'error',
                    html: '<h5>La opcion de problema no es correcta</h5>',
                    showCloseButton: true,
                    focusConfirm: false,
                    confirmButtonText:
                        '<h4>Ok</h4>',
                    confirmButtonAriaLabel: 'Ok'
                });
                return null;
                break;
        }
    }
}
//Control de Nombres de Archivos de Instancia
//NOTA:
/**
 * Hay que hacerlo aparte por como están nombradas las instancias
 */
//Comparar Strings
function PcompareStrings(str1, str2){
    var chunkRgx = /(_+)|([0-9]+)|([^0-9_]+)/g;
    var ax = [], bx = [];
    
    str1.replace(chunkRgx, function(_, $1, $2, $3) {
        ax.push([$1 || "0", $2 || Infinity, $3 || ""])
    });
    str2.replace(chunkRgx, function(_, $1, $2, $3) {
        bx.push([$1 || "0", $2 || Infinity, $3 || ""])
    });
    
    while(ax.length && bx.length) {
        var an = ax.shift();
        var bn = bx.shift();
        var nn = an[0].localeCompare(bn[0]) || 
                 (an[1] - bn[1]) || 
                 an[2].localeCompare(bn[2]);
        if(nn) return nn;
    }
    
    return ax.length - bx.length;
}