def cargarDatosSegunProblemaYRepresentacion(request):
    """
    
    """
    representacion =  request.form["representacion"]
    problema = request.form["problema"]

    datos = {}

    if request.form.get("chbxElitismo"):
        elitismo = 1
    else:
        elitismo = 0

    datos = {
                "poblacion_inicial":int(request.form["poblacion_inicial"]),
                "probabilidad_cruce":float(request.form["probabilidad_cruce"]),
                "probabilidad_mutacion":float(request.form["probabilidad_mutacion"]),
                "lambda_cantidad_de_hijos":int(request.form["lambda_cantidad_de_hijos"]),
                "cantidad_generaciones":int(request.form["cantidad_generaciones"]),
                "cantidad_ejecuciones":int(request.form["cantidad_ejecuciones"]),
                "operador_seleccion":int(request.form["selectSeleccion"]),
                "operador_seleccion_para_reemplazo":int(request.form["selectSeleccionReemplazo"]),
                "operador_cruce":int(request.form["selectCruce"]),
                "operador_mutacion":int(request.form["selectMutacion"]),
                "operador_reemplazo":int(request.form["selectReemplazo"]),
                "elitismo":elitismo,
                "representacion":representacion
            }
    ################################################################################################
    if(representacion=="binario"):

        if(problema == "sat"):
            datos["cantidad_puntos_cruce"]=int(request.form["cantidadPuntos"])

            if "selectInstancia" in request.form:
                datos["instancia"]=request.form["selectInstancia"]

    ################################################################################################
    elif(representacion=="real"):

        if(problema=="optimizacionFunciones"):

            datos["valor_alfa"]=float(request.form["valorAlfa"])
            datos["funcion"]=int(request.form["funciones"])
            datos["tam_dimension"]=int(request.form["tam_dimension"])
            datos["lim_inferior"]=float(request.form["limiteInferior"])
            datos["lim_superior"]=float(request.form["limiteSuperior"])
    #################################################################################################
    elif(representacion=="permutacion"):

        if(problema=="fssp"):
            if "selectInstancia" in request.form:
                datos["instancia"]=request.form["selectInstancia"]

    ##################################################################################################

    else:
        return None

    return datos